#pragma once
#include "Piece.h"
#include <array>
#include <optional>


class Board 
{
public:
	static const size_t height = 4; //sunt dependente de clasa, nu de obiect, de aia trebuie facute statice
	static const size_t width = 4;
	static const size_t size = height * width;
	using Position = std::pair<uint8_t, uint8_t>;
public:
	Board() = default;
	std::optional<Piece>& operator[](const Position& index);
	const std::optional<Piece>& operator[](const Position& index) const; //const ul de la final - piesa returnata nu poate fi modificata


	friend std::ostream& operator<<(std::ostream&, const Board&);

private:
	std::array<std::optional<Piece>, size> m_pieces;//optional s a folosit pentru a salva piesele sau 
	//in cazul optional in care tabela este vida
};