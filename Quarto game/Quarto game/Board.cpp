#include "Board.h"

const char EmptyBoardSpace = '_';

std::optional<Piece>& Board::operator[](const Position& index)
{
    /*uint8_t line = index.first;
    uint8_t column = index.second;*/

    const auto& [line, column] = index;
    return m_pieces[line * Board::width + column];

}


const std::optional<Piece>& Board::operator[](const Position& index) const
{
    /*uint8_t line = index.first;
    uint8_t column = index.second;*/

    const auto& [line, column] = index;
    if (line >= height || column >= width) {
        throw "Out of bounds";
    }
    return m_pieces[line * Board::width + column];

}

std::ostream& operator<<(std::ostream& out, const Board& board)
{
    Board::Position position;

    auto& [line, column] = position;

    for (int line = 0; line < Board::height; line++) {
        for (int column = 0; column < Board::width; column++) {
            if (board[position]) {
                out << *board[{line, column}];
            }
            else {
                out << EmptyBoardSpace << " ";
            }
        }
        std::cout << std::endl;
    }
    return out;
}
