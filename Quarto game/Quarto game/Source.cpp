#include <iostream>
#include "Piece.h"
#include "Board.h"
#include "UnusedPieces.h"


int main() {

	Piece p(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
	std::cout << "Full dark square : " << p << std::endl;
	Board board;
	std::cout << board << std::endl;
	board[{0, 0}] = std::move(p);
	std::cout << board;

	UnusedPieces pieces;
	std::cout << pieces << std::endl;
	pieces.PickPiece("0000");
	std::cout << pieces;

	system("pause");
	return 0;
}