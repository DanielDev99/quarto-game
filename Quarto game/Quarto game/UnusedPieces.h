#pragma once
#include<unordered_map>
#include "Piece.h"


class UnusedPieces
{
	//scopul clasei este genereze toate piesele si sa se verifice daca o anumita piesa a fost
	//sau nu folosita
	//map ul este un container care a fost introdus in c++11 si care are operatiile de insertie 
	//stergere si modificare in complexitate de arbore AVL, functioneaza pe principiul cheie-valoare
	//cheile sunt ordonate automat la fiecare introducere
public:
	UnusedPieces();
	friend std::ostream& operator<<(std::ostream& out, const UnusedPieces&);
	Piece PickPiece(const std::string&);
private:
	std::unordered_map<std::string, Piece> m_unmap;

};