#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape):m_body(body),m_color(color),m_height(height),m_shape(shape)
{
    static_assert(sizeof(*this) <= 1, "This class should be 1 byte in size");
}

Piece::Piece(Piece&&otherPiece)
{
    *this = std::move(otherPiece);
}

Piece& Piece::operator=(const Piece& otherPiece)
{
    m_body = otherPiece.m_body;
    m_color = otherPiece.m_color;
    m_height = otherPiece.m_height;
    m_shape = otherPiece.m_shape;

    return *this;
}
//cand apelam move ul ..
Piece& Piece::operator=(Piece&& otherPiece)
{
    m_body = otherPiece.m_body;
    m_color = otherPiece.m_color;
    m_height = otherPiece.m_height;
    m_shape = otherPiece.m_shape;
    //creeaza o noua piesa cu elementele dat ca parametru, adica otherPiece, el retine ca si obiectul curent si
    //otherPiece au aceealeasi valori(aceeasi zona de memorie de fapt)
    new (&otherPiece) Piece;

    return *this;
}

Piece::Color Piece::GetColor() const
{
    return this->m_color;
}

Piece::Shape Piece::GetShape() const
{
    return this->m_shape;
}

Piece::Body Piece::GetBody() const
{
    return this->m_body;
}

Piece::Height Piece::GetHeight() const
{
    return this->m_height;
}

std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
    return out << static_cast<int>(piece.m_body) << static_cast<int>(piece.m_color) << static_cast<int>(piece.m_shape)
        << static_cast<int>(piece.m_height);
}
