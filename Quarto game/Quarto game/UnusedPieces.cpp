#include "UnusedPieces.h"
#include<bitset>
#include<sstream>

UnusedPieces::UnusedPieces()
{
	//explicatie for : 
	//itereaza 16 valori de la 0 la 15, 0u pentru a semnala ca sunt valori de tip unsigned
	//in acel bitset converteste valorea 0 pe 4 biti si rezulta 0000
	//si o sa vina
	//0000
	//0001
	//0010
	//etc
	//Body(0),Color(0),Height(0),Shape(0)..
	for (auto i = 0u; i < 16u; ++i) {
		auto const bitSet = std::bitset<4>(i);
		Piece piece = Piece(Piece::Body(bitSet[0]), Piece::Color(bitSet[1]), Piece::Height(bitSet[2]), Piece::Shape(bitSet[3]));
		std::stringstream stringStream;
		stringStream << piece;
		m_unmap.insert(std::make_pair(stringStream.str(),std::forward<Piece>(piece)));
	}

	
}

Piece UnusedPieces::PickPiece(const std::string& pieceName)
{
	auto piece = m_unmap.extract(pieceName);
	//functia move ia adresa unei variabile si o atribuie unei alte adrese
	if (piece) {
		return std::move(piece.mapped());
		//cand am apelat functia de extract, ni se returneaza un obiect de tipul cheie valoare,piece contine atat cheia 
		//cat si valoarea iar pentru ca noi sa luam doar valoarea, exista functia de mapped
	}
	else {
		throw "piece not found";
	}
}

std::ostream& operator<<(std::ostream& out, const UnusedPieces&unusedPiece)
{
	for (const auto& [pieceName, piece] : unusedPiece.m_unmap)
	{
		out << pieceName << " ";
	}


	return out;
}
