#pragma once
#include<iostream>
#include<cstdint>

class Piece {
public:
	enum class Color:uint8_t {
		Dark,
		Light
	};
	enum class Shape :uint8_t {
		Round,
		Square
	};
	enum class Body :uint8_t {
		Full,
		Hollow
	};
	enum class Height :uint8_t {
		Short,
		Tall
	};
public:
	Piece() = default;
	Piece(Body body, Color color, Height height, Shape shape);
	//constructor care primeste un rvalue de tipul clasei
	Piece(Piece&&);
	Piece& operator = (const Piece&);
	Piece& operator = (Piece &&);

	Color GetColor() const;
	Shape GetShape() const;
	Body GetBody() const;
	Height GetHeight() const;

	friend std::ostream& operator<<(std::ostream& out, const Piece&);

private:
	Color m_color :1; //:1 nr de biti pe care vrem sa fie salvat acest membru
	Shape m_shape : 1;
	Body m_body :1;
	Height m_height :1;
};